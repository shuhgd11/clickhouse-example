### 1、特点

- 请求有序：同一客户端发起的事务请求，严格按照顺序应用到 Zookeeper 中
- 原子更新：所有事务请求的处理结果在整个集群中所有节点上的应用情况是一致的，整个集群中的所有机器要么都成功，要么都失败
- 数据一致：集群中每个节点都拥有相同的数据副本，客户端连接至任一节点数据都是一致的
- 数据可靠：更改请求成功应用之后，结果将会持久化



### 2、配置

#### 2.1、配置示例

```
# The number of milliseconds of each tick
tickTime=2000
# The number of ticks that the initial 
# synchronization phase can take
initLimit=10
# The number of ticks that can pass between 
# sending a request and getting an acknowledgement
syncLimit=5
# the directory where the snapshot is stored.
# do not use /tmp for storage, /tmp here is just 
# example sakes.
dataDir=/home/zk-example/zk01/data
# the port at which the clients will connect
clientPort=12181
# the maximum number of client connections.
# increase this if you need to handle more clients
#maxClientCnxns=60
#
# Be sure to read the maintenance section of the 
# administrator guide before turning on autopurge.
#
# http://zookeeper.apache.org/doc/current/zookeeperAdmin.html#sc_maintenance
#
# The number of snapshots to retain in dataDir
#autopurge.snapRetainCount=3
# Purge task interval in hours
# Set to "0" to disable auto purge feature
#autopurge.purgeInterval=1
server.1=0.0.0.0:12888:13888
server.2=192.168.200.143:22888:23888
server.3=192.168.200.143:32888:33888:observer

#admin.serverPort=18080
admin.enableServer=false
```



#### 2.2、常规配置

| 配置               | 说明                                                         |
| ------------------ | ------------------------------------------------------------ |
| tickTime           | 通信心跳时间，Zookeeper 服务器与客户端心跳时间，单位毫秒     |
| initLimit          | Leader 和 Follower 初始连接时能容忍的最多心跳数，即 tickTime 的数量 |
| syncLimit          | Leader 和 Follower 同步通信时限，Leader 和 Follower 之间通信时间如果超过 syncLimit * tickTime，Leader 认为 Follwer 故障，从服务器列表中删除 Follwer |
| dataDir            | 数据目录，默认为 tmp 目录，容易被 Linux 定期删除，一般不用默认的 tmp 目录 |
| clientPort         | 客户端连接端口                                               |
| admin.serverPort   | 管理控制台端口，默认 8080（3.5 版本之后新增的功能）          |
| admin.enableServer | 是否开启管理控制台（3.5 版本之后新增的功能）                 |



#### 2.3、集群配置

（1）、节点类型

- Leader：主节点，处理集群中的所有事务请求，集群中只有一个 Leader
- Follower：从节点，处理读请求，参与 Leader 选举
- Observer：观察节点，处理读请求，不参与 Leader 选举

（2）、在各个节点的数据目录下创建 myid 文件，并配置服务器编号，服务器编号需与 zoo.cfg 中的 server 对应

![](md/1.jpg)

（3）、zoo.cfg 配置节点信息

```
server.1=0.0.0.0:12888:13888
server.2=192.168.200.143:22888:23888
server.3=192.168.200.143:32888:33888:observer
```

格式：server.A=B:C:D:observer

- A：服务器编号，需与数据目录下的 myid 文件配置值一致，zk 启动时读取 myid 文件，与 zoo.cfg 中的节点配置信息做对比从而判断具体 server
- B：节点所在服务器地址
- C：Follower 与 Leader 通信端口
- D：Leader 选举端口
- observer：该节点不参与 Leader 选举



#### 2.4、常用操作命令

- 启动 zk：bin/zkServer.sh start
- 停止 zk：bin/zkServer.sh stop
- 查看状态：bin/zkServer.sh status
- 启动客户端：bin/zkCli.sh（quit 退出）





### 3、ZAB（原子广播）

支持崩溃恢复的一致性协议，保证写操作的一致性与可用性，Leader 写入本地日志后复制到各个 Follower 节点



#### 3.1、写 Leader（消息广播）

- 客户端向 Leader发起请求
- Leader 写入本地日志后将写请求发送给各个 Follower，等待 Follower 的 ACK
- Follower 收到 Leader 的写请求后写入本地，返回 ACK
- Leader 接收到半数以上的 ACK 后向所有 Follower 和 Observer 发送 Commit 指令
  - Leader 不需要 Observer 的 ACK，Observer 本身无投票权
  - Leader 本身就有一个 ACK，半数以上包含该 ACK
  - Observer 虽无投票权，但仍需同步 Leader 的写请求以处理客户端的读请求
- 写请求完成，Leader 将处理结果返回给客户端



#### 3.2、写 Follower / 写 Observer

- 客户端对 Follower 或 Observer 节点的写请求将由对应的节点转发至 Leader 节点
- 转发至 Leader 节点后的流程与写 Leader 一致



#### 3.3、读请求

Leader、Follower、Observer 节点均可处理读请求，节点越多整体可处理的读请求量越大



#### 3.4、Leader 选举（崩溃恢复）

##### 3.4.1、节点状态

- LOOKING：Leader 选举中的状态
- FOLLOWING：从节点状态
- LEADING：主节点状态
- OBSERVING：观察者状态



##### 3.4.2、选票信息

- logicClock：逻辑时钟，自增的整数，表示该服务器的投票轮数
- state：当前节点状态
- self_id：当前节点的 myid
- self_zxid：当前节点的最大 zxid
- vote_id：选票节点的 myid
- vote_zxid：选票节点的最大 zxid



##### 3.4.3、选举规则

1. 自增选举轮次：logicClock 自增
2. 初始化自身选票：清空选票记录，投票给自身节点，选票内容为 （vote_id，vote_zxid）
3. 发送选票：广播选票给其他节点
4. 接收外部选票：尝试获取其他节点广播的选票
5. 选举轮次对比
   - 外部选票 logicClock 比自身选票 logicClock 大，说明自身选票选举轮次落后，此时更改自身 logicClock 与 外部 logicClock 一致，等待选票进一步对比
   - 外部选票 logicClock 比自身选票 logicClock 小，说明外部选票选举轮次落后，此时忽略该外部选票
   - 外部选票 logicClock 与自身选票 logicClock 一致，说明是同一轮选举，等待选票进一步对比

6. 选票对比
   - 外部选票 zxid 大于自身选票 zxid，则变更自身选票，选择 zxid 较大的选票
   - 外部选票 zxid 与自身选票 zxid 相同，选择 sid 较大的选票，视情况进行选票更改
7. 再次发送变更的选票
8. 统计投票：选出超过半数的选票节点作为主节点，如果无满足条件的选票则从步骤 3 开始重新对比选票
9. 更新各个节点状态



- Follower 重启或网络故障后，进入 LOOKING 状态重新发起投票，其他节点收到该节点投票后会返回 Leader 和自身投票信息给该节点，该节点重新进入 FOLLOWING 状态
- Leader 宕机 Follower 重新选主，则按上述选举规则重新进行选主
  - Follower 会主动将最大的 zxid 发送给新 Leader，新 Leader 将各个 Follower 最大的 zxid 与自身最大 zxid 之间的所有已经 Commit 的信息同步给各个 Follower
  - 同步完数据后，新 Leader 发送 NEWLEADER 指令，等待各个 Follower 做出 ACK
  - Follower 接收到 NEWLEADER 指令后做出 ACK
  - 新 Leader 接收到半数以上（包括自身）的 ACK 后即可通知各个节点可对外提供服务
  - 新 Leader 存在未 Commit 的记录，如果该记录已超过半数节点接收到，则新 Leader 会发送 Commit 指令，如果未超过半数节点接收，则新 Leader 会通知其他 Follower 节点删除该记录
- 旧 Leader 恢复后，进入 LOOKING 状态重新发起投票，其他节点收到旧 Leader 投票后会返回新 Leader 和自身投票信息给该节点，然后旧 Leader 进入 FOLLOWING 状态



### 4、数据结构

数据模型采用层次化的多叉树形结果，节点可以存储数据（数字、字符串、二进制序列），也可以拥有 N 个子节点，根目录为 "/"



#### 4.1、znode 

zookeeper 中数据的最小单元，每个 znode 对应一个唯一路径标识，节点存储的数据上限为 1M



##### 4.1.1、节点类型

- 持久节点（PERSISTENT）：一旦创建就一直存在即使 ZooKeeper 集群宕机，直到将其删除
- 临时节点（EPHEMERAL）：临时节点的生命周期是与客户端会话（session）绑定的，会话消失则节点消失，临时节点只能做叶子节点不能创建子节点
- 持久顺序节点（PERSISTENT_SEQUENTIAL）：除了具有持久节点的特性之外， 子节点的名称还具有顺序性，由 10 位数字组成的数字串，从 0 开始计数
  - 如：/node1/app0000000001、/node1/app0000000002
- 临时顺序节点（EPHEMERAL_SEQUENTIAL）：除了具有临时节点的特性之外，子节点的名称还具有顺序性



##### 4.1.2、znode 数据结构

<img src="md/2.png" style="zoom:80%;" />

- czxid：创建节点的事务 zxid
- ctime：znode 被创建的毫秒数（从 1970 年开始）
- mZxid：znode 最后更新的事务 zxid
- mtime：znode 最后修改的毫秒数（从 1970 年开始）
- pZxid：znode 最后更新的子节点 zxid
- cversion：znode 子节点变化号，znode 子节点修改次数
- dataVersion：znode 数据变化号
- aclVersion：znode 访问控制列表变化号
- ephemeralOwner：如果是临时节点为 znode 的 sessionId，非临时节点则固定为 0
- dataLength：znode 数据长度
- numChildren：znode 子节点数量





### 5、应用

#### 5.1、分布式锁

##### 5.1.1、非公平锁

- 临时节点 + watch 机制
- 若临时节点可创建成功，则说明获取锁成功，否则加锁失败进入阻塞
- 加锁失败的节点 watch 该节点
  - 释放锁时会删除该临时节点，watch 回调尝试获取锁
  - 持有锁的客户端故障，session 关闭该临时节点也会被删除，不会产生死锁
- 锁释放后，其他等待的客户端都能收到通知共同竞争锁资源，如果等待锁资源的客户端过多，则 zookeeper 的 watch 机制需通知所有相关客户端（羊群效应，zookeeper 负担较重）



##### 5.1.2、公平锁

- apache curator 可重入排它锁，InterProcessMutex
- 临时顺序节点 + watch 机制
- 加锁时创建临时顺序节点，若创建的临时顺序节点 id 最小则说明加锁成功，否则加锁失败进入阻塞
  - 通过 synchronized 实现互斥阻塞（wait()），加锁对象为当前锁对象 LockInternals.this
  - ConcurrentHashMap 维护线程锁信息，k 为线程信息，v 为锁信息，利用 synchronized 实现锁重入，维护 lockCount 加锁次数

- 加锁失败的节点只 watch 上一个临时顺序节点，释放锁时 zookeeper 只需通知加锁节点的下一个临时顺序节点
  - 释放锁通过锁对象 LockInternals.this 唤醒所有线程（notifyAll），每个线程检查自身是有为顺序节点的首节点，是则获取锁，否则再次进入阻塞


```java
private boolean internalLockLoop(long startMillis, Long millisToWait, String ourPath) throws Exception {
	boolean haveTheLock = false;
    boolean doDelete = false;
    try {
    	if (revocable.get() != null) {
        	client.getData().usingWatcher(revocableWatcher).forPath(ourPath);
       	}
        // 获取锁成功或客户端异常退出循环
		while ((client.getState() == CuratorFrameworkState.STARTED) && !haveTheLock) {
            // 获取排好序的子节点
        	List<String> children = getSortedChildren();
          	String sequenceNodeName = ourPath.substring(basePath.length() + 1);
			PredicateResults predicateResults = driver.getsTheLock(client, children, sequenceNodeName, maxLeases);
            // 判断是否为临时顺序节点的首节点，是则获取锁成功
           	if (predicateResults.getsTheLock()) {
            	haveTheLock = true;
            } else {
                // 获取锁失败，获取上一节点路径
               	String previousSequencePath = basePath + "/" + predicateResults.getPathToWatch();
				synchronized (this) {
                	try {
						// 加锁，监听上一节点
                      	client.getData().usingWatcher(watcher).forPath(previousSequencePath);
                       	if (millisToWait != null) {
                        	millisToWait -= (System.currentTimeMillis() - startMillis);
                          	startMillis = System.currentTimeMillis();
                          	if (millisToWait <= 0) {
                            	doDelete = true;
                            	break;
                           	}
                            // 进入阻塞等待唤醒，有限等待
							wait(millisToWait);
                    	} else {
                            // 进入阻塞等待唤醒，无限等待
                       		wait();
                       	}
                 	} catch (KeeperException.NoNodeException e) {}
               	}
            }
        }
  	} catch (Exception e) {
        // 异常捕获，包括 wait 超时或打断异常
      	ThreadUtils.checkInterrupted(e);
        // 标记删除自身节点
       	doDelete = true;
       	throw e;
   	} finally {
      	if (doDelete) {
            // 加锁失败，删除自身节点
        	deleteOurPath(ourPath);
       	}
   	}
  	return haveTheLock;
}
```



##### 5.1.3、读写锁

- 永久节点 + 临时顺序节点 + watch 机制
- 永久节点作为加锁跟节点
- 加读锁时在该永久节点下创建临时顺序节点并标记为读锁（赋值为 0），若创建的临时顺序节点 id 最小或该临时顺序节点之前的节点锁标记都读锁（值为 0），则获取读锁成功，否则获取读锁失败进入阻塞
- 获取读锁失败时，只 watch 最近的写锁节点，等待该节点执行完成释放锁资源
- 加写锁时在该永久节点下创建临时顺序节点并标记为写锁（赋值为 1），若创建的临时顺序节点 id 最小，则获取写锁成功，否则获取写锁失败进入阻塞
- 获取写锁失败时，watch 最近的锁节点，等待该节点执行完成释放锁资源



#### 5.2、Leader 选举

##### 5.2.1、Leader Latch

- apache curator - Leader Latch
- 临时顺序节点 + watch 机制
- 指定路径创建临时顺序节点，获取 id 最小的客户端即为 Leader
- 非 Leader 的客户端 watch 自身临时顺序节点的上一节点
- Leader 节点删除后重新触发选举



### 6、扩展

#### 6.1、两阶段提交（2PC）

- 一阶段：提交事务请求
  - 协调者向所有参与者发送事务请求，等待参与者响应
  - 参与者执行事务操作，做出响应
- 二阶段：执行事务提交
  - 协调者根据参与者的反馈选择提交事务还是回滚事务
  - 参与者根据接收到的命名执行事务提交或者事务回滚，做出响应
  - 协调者接收响应，结束
- 缺点
  - 同步阻塞：协调者需等待所有参与者的响应，参与者收到之后如果能处理那么它将会进行事务的处理但并不提交，这个时候会一直占用着资源不释放
  - 单点故障：协调者存在单点故障问题
  - 数据不一致：参与者并没有都接收到 commit 命令，导致数据不一致



#### 6.2、三阶段提交（3PC）

- 一阶段：事务询问
  - 协调者向参与者发送事务请求，等待参与者响应
  - 参与者接收到事务请求做出响应，但未执行事务具体操作，只是进入预备状态
- 二阶段：预提交
  - 协调者根据一阶段响应选择继续或者中断
  - 请求中断
    - 一阶段有参与者响应失败，或者存在参与者响应超时，发起中断请求
    - 参与者接收到中断请求后进行事务中断（协调者响应超时也会中断）
  - 预提交
    - 协调者向参与者发送预提交请求，等待参与者响应
    - 参与者接收到预提交请求后执行事务内容，做出响应
- 三阶段：提交事务
  - 协调者根据二节点响应选择继续或者中断
  - 请求中断
    - 二阶段有参与者响应失败，或者存在参与者响应超时，发起中断请求
    - 参与者接收到中断请求后进行事务回滚，做出响应
    - 协调者接收回滚结果，事务结束
  - 提交
    - 协调者发送最终提交命令，等待响应
    - 参与者接收到提交命令，执行事务提交，做出响应（如果参与者在一定时间内未接收到协调者的提交命令，仍然会提交）
    - 协调者接收到所有参与者的响应后，最终完成事务提交
- 只缓解了一致性问题，单点故障，数据不一致仍然存在